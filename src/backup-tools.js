#!/usr/bin/env node

'use strict';

exports = module.exports = {
    encrypt,
    decrypt,
    encryptFilename,
    decryptFilename,
    decryptDir
};

const assert = require('assert'),
    async = require('async'),
    crypto = require('crypto'),
    debug = require('debug')('cloudron-backup'),
    fs = require('fs'),
    path = require('path'),
    safe = require('safetydance'),
    TransformStream = require('stream').Transform;

function encryptFilePath(filePath, encryption) {
    assert.strictEqual(typeof filePath, 'string');
    assert.strictEqual(typeof encryption, 'object');

    var encryptedParts = filePath.split('/').map(function (part) {
        let hmac = crypto.createHmac('sha256', Buffer.from(encryption.filenameHmacKey, 'hex'));
        const iv = hmac.update(part).digest().slice(0, 16); // iv has to be deterministic, for our sync (copy) logic to work
        const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(encryption.filenameKey, 'hex'), iv);
        let crypt = cipher.update(part);
        crypt = Buffer.concat([ iv, crypt, cipher.final() ]);

        return crypt.toString('base64')     // ensures path is valid
            .replace(/\//g, '-')            // replace '/' of base64 since it conflicts with path separator
            .replace(/=/g,'');              // strip trailing = padding. this is only needed if we concat base64 strings, which we don't
    });

    return encryptedParts.join('/');
}

function decryptFilePath(filePath, encryption) {
    assert.strictEqual(typeof filePath, 'string');
    assert.strictEqual(typeof encryption, 'object');

    let decryptedParts = [];
    for (let part of filePath.split('/')) {
        part = part + Array(part.length % 4).join('='); // add back = padding
        part = part.replace(/-/g, '/');                 // replace with '/'

        try {
            const buffer = Buffer.from(part, 'base64');
            const iv = buffer.slice(0, 16);

            const decrypt = crypto.createDecipheriv('aes-256-cbc', Buffer.from(encryption.filenameKey, 'hex'), iv);
            const plainText = decrypt.update(buffer.slice(16));
            const plainTextString = Buffer.concat([ plainText, decrypt.final() ]).toString('utf8');
            const hmac = crypto.createHmac('sha256', Buffer.from(encryption.filenameHmacKey, 'hex'));
            if (!hmac.update(plainTextString).digest().slice(0, 16).equals(iv)) return { error: new Error(`mac error decrypting part ${part} of path ${filePath}`) };

            decryptedParts.push(plainTextString);
        } catch (error) {
            debug(`Error decrypting file ${filePath} part ${part}:`, error);
            return null;
        }
    }

    return { decryptedFilePath: decryptedParts.join('/') };
}

class EncryptStream extends TransformStream {
    constructor(encryption) {
        super();
        this._headerPushed = false;
        this._iv = crypto.randomBytes(16);
        this._cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(encryption.dataKey, 'hex'), this._iv);
        this._hmac = crypto.createHmac('sha256', Buffer.from(encryption.dataHmacKey, 'hex'));
    }

    pushHeaderIfNeeded() {
        if (!this._headerPushed) {
            const magic = Buffer.from('CBV2');
            this.push(magic);
            this._hmac.update(magic);
            this.push(this._iv);
            this._hmac.update(this._iv);
            this._headerPushed = true;
        }
    }

    _transform(chunk, ignoredEncoding, callback) {
        this.pushHeaderIfNeeded();

        try {
            const crypt = this._cipher.update(chunk);
            debug('Pushed:', crypt.toString('hex'));
            this._hmac.update(crypt);
            callback(null, crypt);
        } catch (error) {
            callback(error);
        }
    }

    _flush(callback) {
        try {
            this.pushHeaderIfNeeded(); // for 0-length files
            const crypt = this._cipher.final();
            this.push(crypt);
            debug('Pushed:', crypt.toString('hex'));
            this._hmac.update(crypt);
            const mac = this._hmac.digest();
            debug('Pushed mac:', mac.toString('hex'));
            callback(null, mac);
        } catch (error) {
            callback(error);
        }
    }
}

class DecryptStream extends TransformStream {
    constructor(encryption) {
        super();
        this._key = Buffer.from(encryption.dataKey, 'hex');
        this._header = Buffer.alloc(0);
        this._decipher = null;
        this._hmac = crypto.createHmac('sha256', Buffer.from(encryption.dataHmacKey, 'hex'));
        this._buffer = Buffer.alloc(0);
    }

    _transform(chunk, ignoredEncoding, callback) {
        const needed = 20 - this._header.length; // 4 for magic, 16 for iv

        debug('got chunk', chunk.length);
        if (this._header.length !== 20) { // not gotten IV yet
            this._header = Buffer.concat([this._header, chunk.slice(0, needed)]);
            if (this._header.length !== 20) return callback();

            if (!this._header.slice(0, 4).equals(new Buffer.from('CBV2'))) return callback(new Error('Invalid magic in header'));

            const iv = this._header.slice(4);
            this._decipher = crypto.createDecipheriv('aes-256-cbc', this._key, iv);
            this._hmac.update(this._header);
        }

        debug('needed is', needed);
        this._buffer = Buffer.concat([ this._buffer, chunk.slice(needed) ]);
        debug('buffer is ', this._buffer.length);
        if (this._buffer.length < 32) return callback();

        try {
            const cipherText = this._buffer.slice(0, -32);
            debug('Got:', cipherText.toString('hex'));
            this._hmac.update(cipherText);
            const plainText = this._decipher.update(cipherText);
            this._buffer = this._buffer.slice(-32);
            callback(null, plainText);
        } catch (error) {
            callback(error);
        }
    }

    _flush (callback) {
        if (this._buffer.length !== 32) return callback(new Error('Invalid password or tampered file (not enough data)'));

        try {
            debug('Expected mac:', this._buffer.toString('hex'));
            const mac = this._hmac.digest();
            debug('Computed Mac:', mac.toString('hex'));
            if (!mac.equals(this._buffer)) return callback(new Error('Invalid password or tampered file (mac mismatch)'));

            const plainText = this._decipher.final();
            callback(null, plainText);
        } catch (error) {
            callback(error);
        }
    }
}

function exit(msgOrError) {
    if (typeof msgOrError === 'string') process.stderr.write(`Error: ${msgOrError}\n`);
    else if (msgOrError instanceof Error) process.stderr.write(`Error: ${msgOrError.message}\n`);

    process.exit(msgOrError ? 1 : 0);
}

function aesKeysFromPassword(password) {
    const derived = crypto.scryptSync(password, Buffer.from('CLOUDRONSCRYPTSALT', 'utf8'), 128);
    return {
        dataKey: derived.slice(0, 32).toString('hex'),
        dataHmacKey: derived.slice(32, 64).toString('hex'),
        filenameKey: derived.slice(64, 96).toString('hex'),
        filenameHmacKey: derived.slice(96).toString('hex')
    };
}

function encrypt(input, options) {
    if (!options.password) return exit('--password is needed');

    const encryption = aesKeysFromPassword(options.password);

    let inStream = fs.createReadStream(input);
    let outStream = process.stdout;
    let encryptStream = new EncryptStream(encryption);

    inStream.on('error', exit);
    encryptStream.on('error', exit);

    inStream.pipe(encryptStream).pipe(outStream);
}

function encryptFilename(filePath, options) {
    if (!options.password) return exit('--password is needed');

    const encryption = aesKeysFromPassword(options.password);

    console.log(encryptFilePath(filePath, encryption));
}

function decrypt(input, options) {
    if (!options.password) return exit('--password is needed');

    const fd = safe.fs.openSync(input, 'r');
    if (!fd) return exit(safe.error);
    let header = Buffer.alloc(4);
    if (!safe.fs.readSync(fd, header, 0, 4, 0)) return exit(safe.error);
    if (!header.equals(Buffer.from('CBV2'))) return exit('Legacy stream decryption not implemented yet');
    safe.fs.closeSync(fd);

    const encryption = aesKeysFromPassword(options.password);

    let inStream = fs.createReadStream(input);
    let outStream = process.stdout;
    let decryptStream = new DecryptStream(encryption);

    inStream.on('error', exit);
    decryptStream.on('error', exit);

    inStream.pipe(decryptStream).pipe(outStream);
}

function decryptDir(inDir, outDir, options) {
    if (!options.password) return exit('--password is needed');

    const encryption = aesKeysFromPassword(options.password);

    const inDirAbs = path.resolve(process.cwd(), inDir);
    const outDirAbs = path.resolve(process.cwd(), outDir);

    let tbd = [ '' ]; // only has paths relative to inDirAbs
    async.whilst((done) => done(null, tbd.length !== 0), function iteratee(whilstCallback) {
        const cur = tbd.pop();
        const entries = fs.readdirSync(path.join(inDirAbs, cur), { withFileTypes: true });
        async.eachSeries(entries, function (entry, iteratorCallback) {
            if (entry.isDirectory()) {
                tbd.push(path.join(cur, entry.name));
                return iteratorCallback();
            } else if (!entry.isFile()) {
                return iteratorCallback();
            }

            const encryptedFilePath = path.join(cur, entry.name);
            const { error, decryptedFilePath } = decryptFilePath(encryptedFilePath, encryption);
            if (error) return iteratorCallback(error);

            let inStream = fs.createReadStream(path.join(inDirAbs, cur, entry.name));
            fs.mkdirSync(path.dirname(path.join(outDirAbs, decryptedFilePath)), { recursive: true });
            let outStream = fs.createWriteStream(path.join(outDirAbs, decryptedFilePath));
            let decryptStream = new DecryptStream(encryption);

            inStream.on('error', iteratorCallback);
            decryptStream.on('error', iteratorCallback);

            inStream.pipe(decryptStream).pipe(outStream).on('finish', iteratorCallback);
        }, whilstCallback);
    }, exit);
}

function decryptFilename(filePath, options) {
    if (!options.password) return exit('--password is needed');

    const encryption = aesKeysFromPassword(options.password);

    const { error, decryptedFilePath } = decryptFilePath(filePath, encryption);
    if (error) return exit(error);
    console.log(decryptedFilePath);
}
