'use strict';

exports = module.exports = {
    build: build
};

const assert = require('assert'),
    config = require('./config.js'),
    crypto = require('crypto'),
    EventSource = require('eventsource'),
    execSync = require('child_process').execSync,
    exit = require('./helper.js').exit,
    fs = require('fs'),
    once = require('once'),
    helper = require('./helper.js'),
    manifestFormat = require('cloudron-manifestformat'),
    micromatch = require('micromatch'),
    readlineSync = require('readline-sync'),
    superagent = require('superagent'),
    os = require('os'),
    path = require('path'),
    safe = require('safetydance'),
    tar = require('tar-fs'),
    url = require('url'),
    util = require('util');

function superagentEndBuildService(requestFactory, callback) {
    assert.strictEqual(typeof requestFactory, 'function');
    assert.strictEqual(typeof callback, 'function');

    requestFactory().end(function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode === 400) return authenticateBuildService({ error: true }, superagentEndBuildService.bind(null, requestFactory, callback));
        if (result.statusCode === 401) return authenticateBuildService({ error: true }, superagentEndBuildService.bind(null, requestFactory, callback));
        if (result.statusCode === 403) return callback(new Error(result.type === 'application/javascript' ? JSON.stringify(result.body) : result.text));
        callback(error, result);
    });
}

function authenticateBuildService(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    var buildServiceConfig = config.getBuildServiceConfig();

    if (!options.hideBanner) console.log('Build Service login' + ` (${buildServiceConfig.url}):`);

    var username = options.username || readlineSync.question('Username: ', {});
    var password = options.password || readlineSync.question('Password: ', { noEchoBack: true });

    // reset token
    buildServiceConfig.token = null;
    config.setBuildServiceConfig(buildServiceConfig);

    superagent.post(`${buildServiceConfig.url}/api/v1/login`).send({ username: username, password: password }).end(function (error, result) {
        if (error && !error.response) exit(error);

        if (result.statusCode !== 200 && result.statusCode !== 201) {
            console.log('Login failed.');

            options.hideBanner = true;
            options.username = '';
            options.password = '';

            return authenticateBuildService(options, callback);
        }

        buildServiceConfig.token = result.body.accessToken;
        config.setBuildServiceConfig(buildServiceConfig);

        console.log('Login successful.');

        if (typeof callback === 'function') callback();
    });
}

function followBuildLog(buildId, raw, callback) {
    assert.strictEqual(typeof buildId, 'string');
    assert.strictEqual(typeof raw, 'boolean');

    // ensure callback is only ever called once
    callback = once(callback);

    // EventSource always requires http
    let tmp = url.parse(config.getBuildServiceConfig().url);
    if (tmp.protocol !== 'https:' && tmp.protocol !== 'http:') tmp = url.parse('http://' + config.getBuildServiceConfig().url);

    var es = new EventSource(`${tmp.href}api/v1/builds/${buildId}/logstream?accessToken=${config.getBuildServiceConfig().token}`);
    var prevId = null, prevWasStatus = false;

    es.on('message', function (e) {
        if (raw) return console.dir(e);

        var data = safe.JSON.parse(e.data);
        if (!data) return; // this is a bug in docker or our build server

        if (data.status) { // push log
            if (data.id && data.id === prevId) {
                // the code below does not work os x if the line wraps, maybe we should clip the text to window size?
                process.stdout.clearLine();
                process.stdout.cursorTo(0);
            } else if (prevWasStatus) {
                process.stdout.write('\n');
            }

            process.stdout.write(data.status + (data.id ? ' ' + data.id : '') + (data.progress ? ' ' + data.progress : ''));

            prevId = data.id;
            prevWasStatus = true;

            return;
        }

        if (prevWasStatus === true) {
            process.stdout.write('\n');
            prevId = null;
            prevWasStatus = false;
        }

        if (data.stream) { // build log
            process.stdout.write(data.stream);
        } else if (data.message) {
            console.log(data.message);
        } else if (data.errorDetail) {
            console.log(data.errorDetail.message ? data.errorDetail.message : data.errorDetail);
        } else if (typeof data.error === 'string') {
            console.log(data.error);
        } else if (data.error) {
            console.error(data.error);
        }
    });
    es.on('error', function (error) {
        if (raw) console.dir(error);

        // We sometimes get { type: 'error' } from es module when the server closes the socket. not clear why
        if (error && !error.status && error.type === 'error') error = null;

        callback(error && error.status ? error : null); // eventsource module really needs to give us better errors
    });
}

function dockerignoreMatcher(dockerignorePath) {
    var patterns = [];

    if (fs.existsSync(dockerignorePath)) {
        patterns = fs.readFileSync(dockerignorePath, 'utf8').split('\n');

        patterns = patterns.filter(function (line) { return line[0] !== '#'; });
        patterns = patterns.map(function (line) {
            var l = line.trim();

            while (l[0] === '/') l = l.slice(1);
            while (l[l.length-1] === '/') l = l.slice(0, -1);

            return l;
        });
        patterns = patterns.filter(function (line) { return line.length !== 0; });
    }

    return function ignore(path) {
        return micromatch([ path ], patterns, { dot: true }).length == 1;
    };
}

function buildLocal(manifest, sourceDir, appConfig, options) {
    let tag;
    if (options.tag) {
        tag = options.tag;
    } else {
        // use a simple timestamp as tag. tried to use manifest.version here but this requires the user to remember to
        // bump the manifest version before building (and for approval for us). maybe another idea is to just use a constant 'dev' tag
        const random = crypto.randomBytes(24 / 8).toString('hex');
        tag = new Date().toISOString().replace(/[-:Z]/g, '').replace(/T|\./g, '-') + random;
    }

    const dockerImage = `${appConfig.repository}:${tag}`;

    console.log('Building locally as %s', dockerImage);
    console.log();

    let buildArgsCmdLine = options.buildArg.map(function (a) { return `--build-arg "${a}"`; }).join(' ');

    let dockerfile = 'Dockerfile';
    if (options.file) dockerfile = options.file;
    else if (fs.existsSync(`${sourceDir}/Dockerfile.cloudron`)) dockerfile = 'Dockerfile.cloudron';
    else if (fs.existsSync(`${sourceDir}/cloudron/Dockerfile`)) dockerfile = 'cloudron/Dockerfile';
    execSync(`docker build ${!options.cache ? '--no-cache' : ''} -t ${dockerImage} -f ${dockerfile} ${buildArgsCmdLine} ${sourceDir}`, { stdio: 'inherit' });

    if (options.push) {
        console.log();
        console.log(`Pushing ${dockerImage}`);

        safe.child_process.execSync(`docker push ${dockerImage}`, { stdio: 'inherit' });
        if (safe.error) exit('Failed to push image (are you logged in? if not, use "docker login")');
    }

    let result = safe.child_process.execSync(`docker inspect --format="{{index .RepoDigests 0}}" ${dockerImage}`, { encoding: 'utf8' });
    if (safe.error) exit('Failed to inspect image');
    let match = /.*@sha256:(.*)/.exec(result.trim());
    if (!match) exit('Failed to detect sha256');

    appConfig.dockerImage = `${dockerImage}`;
    appConfig.dockerImageSha256 = match[1]; // stash this separately for now
    config.setAppConfig(sourceDir, appConfig);
}

function buildRemote(manifest, sourceDir, appConfig, options) {
    console.log('Using build service', config.getBuildServiceConfig().url);

    let tag;
    if (options.tag) {
        tag = options.tag;
    } else {
        // use a simple timestamp as tag. tried to use manifest.version here but this requires the user to remember to
        // bump the manifest version before building (and for approval for us). maybe another idea is to just use a constant 'dev' tag
        const random = crypto.randomBytes(24 / 8).toString('hex');
        tag = new Date().toISOString().replace(/[-:Z]/g, '').replace(/T|\./g, '-') + random;
    }

    const dockerImage = `${appConfig.repository}:${tag}`;

    console.log('Building %s', dockerImage);

    var sourceArchiveFilePath = path.join(os.tmpdir(), path.basename(sourceDir) + '.tar.gz');
    var dockerignoreFilePath = path.join(sourceDir, '.dockerignore');
    var ignoreMatcher = dockerignoreMatcher(dockerignoreFilePath);

    console.log('Uploading source tarball...');

    var stream = tar.pack(sourceDir, {
        ignore: function (name) {
            return ignoreMatcher(name.slice(sourceDir.length + 1)); // make name as relative path
        }
    }).pipe(fs.createWriteStream(sourceArchiveFilePath));

    stream.on('error', function (error) {
        exit('Failed to create application source archive: ' + error);
    });

    let dockerfile = 'Dockerfile';
    if (options.file) dockerfile = options.file;
    else if (fs.existsSync(`${sourceDir}/Dockerfile.cloudron`)) dockerfile = 'Dockerfile.cloudron';
    else if (fs.existsSync(`${sourceDir}/cloudron/Dockerfile`)) dockerfile = 'cloudron/Dockerfile';

    const buildArgsObject = {};
    options.buildArg.forEach(function (a) {
        const key = a.slice(0, a.indexOf('='));
        const value = a.slice(a.indexOf('=')+1);
        buildArgsObject[key] = value;
    });

    stream.on('finish', function () {
        superagentEndBuildService(function () {
            let buildServiceConfig = config.getBuildServiceConfig();
            return superagent.post(`${buildServiceConfig.url}/api/v1/builds`)
                .query({ accessToken: buildServiceConfig.token, noCache: !options.cache, dockerfile: dockerfile, noPush: !options.push })
                .field('dockerImageRepo', appConfig.repository)
                .field('dockerImageTag', tag)
                .field('buildArgs', JSON.stringify(buildArgsObject))
                .attach('sourceArchive', sourceArchiveFilePath);
        }, function (error, result) {
            if (error && !error.response) return exit(util.format('Failed to build app: %s', error.message));
            if (result.statusCode === 413) exit('Failed to build app. The app source is too large.\nPlease adjust your .dockerignore file to only include neccessary files.');
            if (result.statusCode !== 201) exit(util.format('Failed to build app (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

            var buildId = result.body.id;

            followBuildLog(buildId, false, function (error) {
                if (error) return console.error(error);

                superagentEndBuildService(function () {
                    let buildServiceConfig = config.getBuildServiceConfig();
                    return superagent.get(`${buildServiceConfig.url}/api/v1/builds/${buildId}`)
                        .query({ accessToken: buildServiceConfig.token });
                }, function (error, result) {
                    if (error && !error.response) return exit(util.format('Failed to build app: %s', error.message));
                    if (result.statusCode !== 200) exit(util.format('Failed to build app (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));
                    if (result.body.status !== 'success') exit('Failed to build app. See log output above.');

                    appConfig.dockerImage = dockerImage;
                    // appConfig.dockerImageSha256 = match[1]; // stash this separately for now
                    config.setAppConfig(sourceDir, appConfig);

                    console.log(dockerImage);
                    console.log('\nBuild successful');

                    exit();
                });
            });
        });
    });
}

function build(options) {
    helper.verifyArguments(arguments);

    // try to find the manifest of this project
    const manifestFilePath = helper.locateManifest();
    if (!manifestFilePath) return exit('No CloudronManifest.json found');

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit('Error in CloudronManifest.json: ' + result.error.message);

    let manifest = result.manifest;
    const sourceDir = path.dirname(manifestFilePath);

    const appConfig = config.getAppConfig(sourceDir);

    let buildService = config.getBuildServiceConfig();
    if (!buildService.type) buildService.type = 'local'; // default

    if (options.local) {
        buildService.type = 'local';
    } else if (options.setBuildService) {
        buildService.token = null;
        buildService.url = null;
        buildService.type = 'remote';

        let url;
        if (typeof options.setBuildService === 'string') {
            url = options.setBuildService;
        } else {
            url = readlineSync.question('Enter build service URL: ', { });
        }

        if (!url.startsWith('https://')) url = `https://${url}`;
        buildService.url = url;
    }

    if (options.buildServiceToken) buildService.token = options.buildServiceToken;

    config.setBuildServiceConfig(buildService);

    let repository = appConfig.repository;
    if (!repository || options.setRepository) {
        if (typeof options.setRepository === 'string') {
            repository = options.setRepository;
        } else {
            repository = readlineSync.question(`Enter docker repository (e.g registry/username/${manifest.id || path.basename(sourceDir)}): `, {});
            if (!repository) exit('No repository provided');
            console.log();
        }

        appConfig.repository = repository;
        config.setAppConfig(sourceDir, appConfig);
    }

    if (buildService.type === 'local') {
        buildLocal(manifest, sourceDir, appConfig, options);
    } else if (buildService.type === 'remote' && buildService.url) {
        buildRemote(manifest, sourceDir, appConfig, options);
    } else {
        exit('Unknown build service type or missing build service url. Rerun with --reset-build-service');
    }
}
