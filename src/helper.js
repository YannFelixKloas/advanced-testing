/* jshint node:true */

'use strict';

const fs = require('fs'),
    path = require('path'),
    util = require('util');

exports = module.exports = {
    exit,

    locateManifest,
    verifyArguments
};

function exit(error) {
    if (error instanceof Error) console.log(error.message);
    else if (error) console.log(util.format.apply(null, Array.prototype.slice.call(arguments)));

    process.exit(error ? 1 : 0);
}

function locateManifest() {
    let curdir = process.cwd();
    do {
        const candidate = path.join(curdir, 'CloudronManifest.json');
        if (fs.existsSync(candidate)) return candidate;

        // check if we can't go further up (the previous check for '/' breaks on windows)
        if (curdir === path.resolve(curdir, '..')) break;

        curdir = path.resolve(curdir, '..');
    // eslint-disable-next-line no-constant-condition
    } while (true);

    return null;
}

function verifyArguments(args) {
    if (args.length > 1) {
        console.log('Too many arguments');
        args[0].parent.help(); // extra args won't have parent() field
        process.exit(1);
    }
}
